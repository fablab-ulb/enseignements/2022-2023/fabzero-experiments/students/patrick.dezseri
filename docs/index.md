## Foreword

Ahoy yokefellows!

This is an example student blog for the [2022-2023 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/).

Each student has a personal GitLab remote repository in the [GitLab class group](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students) in which a template of a blog is already stored. 

The GitLab software  is set to use [MkDocs](https://www.mkdocs.org/) to turn simple text files written in [Markdown](https://en.wikipedia.org/wiki/Markdown) format, into the site you are navigating.

You can join our year group (non-official communication channel) on [discord](https://discord.gg/NS4K86M8sb)

#### Edit your blog

There are several ways to edit your blog

* by navigating your remote GitLab repository and [editing the files using the GitLab Web Editor](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#edit-a-file).
* by using [git](https://rogerdudler.github.io/git-guide/) and Command Line on your computer : cloning a local copy of your online project repository, editing the files locally and synchronizing back your local repository with the copy on the remote server.
* you can see how I did in my Module 1

#### Publishing your blog

Two times a week and each time you change a file, the site is rebuilt and all the changes are published in few minutes.

#### It saves history

No worries, you can't break anything, all the changes you make are saved under [Version Control](https://en.wikipedia.org/wiki/Version_control) using [GIT](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control). This means that you have all the different versions of your page saved and available all the time in the Gitlab interface.

## About me

Hello there! My name is Patrik Dezséri, or Patrick, depends which of my nationality I am using. By that I mean that I am at the same time hungarian and french, but I live in Belgium. Don't ask me why. My pseudo is forestpas3 and for work I usually use DEP as a signature/username.

![This is how I see myself](images/smaller_avatar.jpg){width="300", align=right}
This is how I see myself. I found this drawing in a student made syllabus for a genetics course #Bruno
 


Right now I am a master student in Chemistry and Bio-industries Bioengineering. I am currently doing my MFE in the SFMB laboratory. 

I like gaming, guitar, paleontology, space stuff and cooking. I am also an amateur in technology and building/dismantling electronics. I want also to discover new things and improve my knowledge. That is also why I took this course. 

I am mostly a fantasy and Sci-fi reader. For games I prefer RTS, RPG, Co-Op. I am really bad at FPS. For examples Minecraft, Stellaris, Valheim, Skyrim,...
This is also true for movies. I enjoy Star Wars, Star Trek, Vox Machina, ... 
For musics, I am an old school and listen mostly songs from the [60s, 70s, 80s](https://music.youtube.com/playlist?list=PLyMqWuDkM_9ezylb8S4bIZTEeKlrE_iny&feature=share) and [shanties](https://music.youtube.com/channel/UCPtgd_D0BIXZX2XbDely3BQ).

If you have any questions do not hesitate to ask, as I am also the class rep for this year. 

Contacts:

* patrick.dezseri@ulb.be
* discord : forestpas3#8496
* messenger : Patrik Dezséri (but I am less active there)
* irl : P.BC4 (I am mostly there for my thesis of Ma2, so don't hesitate to come by)
