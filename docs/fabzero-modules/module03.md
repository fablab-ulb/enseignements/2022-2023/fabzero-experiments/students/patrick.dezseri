# Module 3. 3D printing

## 3.1. Slicing software

Download and install [PrusaSlicer](https://help.prusa3d.com/article/install-prusaslicer_1903)

When installing select the 
[type of printers](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/patrick.dezseri/fabzero-modules/module02/#321-type-of-Printers-present-at-the-FabLab)
 that are present at the fablab. 

When asked select Advanced or Expert option.

## 3.2. How to use PrusaSlicer
### 3.2.1. Type of Printers present at the FabLab

There are three type of 3D printers at our Fablab

* Prusa i3 MK3
* Prusa i3 MK3 S
* Prusa i3 MK3 S+

Their specificities are :

* printing surface of (XY) : 21cm x 21cm
* max printing height of (Z): 25cm
* and a nozzle of  0,4mm

### 3.2.2. Creating your printable file

This is a follow up from [Module 2](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/patrick.dezseri/fabzero-modules/module02/#222-creating-the-shape). 

1. First, export your 3D object from the program you used (OpenSCAD, FreeCAD, ...) into an **.stl** file
2. In PrusaSlicer import the stl file

![Import](../images/Mod3/import.png){width="500"}

3. Than we need to position the object on the printing plate with the `Place on Face` tool. We also need to take into account:
   * having the biggest surface contact
   * having the minimum support 

![Import](../images/Mod3/place_face.png){width="500"}

4. The following settings can be selected for printing a prototype:
     * For filament : `Generic PLA` but it needs to be verified if it is PLA that is inserted into the printer. If not it can be changed.
     * For the layer thickness : 0,2mm QUALITY for a faster printing speed, with still having a good quality printing.
     * For the support system it depends on your object,
     * For the infill you can usually use `Gyroid` for a stable structure filling with a 15% filling.

5. After that you have chosen the parameters you can click on `Slice Now`. It will give an estimated printing time. If it is too long you can play with the different parameters. If not you can continue with `Exporte G-code` and copy the exported file into an SD card that can be inserted into the printer.
6. In the printer select your file and start printing. 

My file can be found [here](https://gitlab.com/patrick.dezseri/my-files/-/tree/main/3D-Objects/Spring).

Source:

* [FabLab Guide](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md#pr%C3%A9paration-du-g-code)


## 3.3. Printing 

### 3.3.1. Before printing 

1. Check if the filament is the same as the one specified in the G-code
2. Clean the printing area / plate
     * you can clean it with acetone 

Source :

* [FabLab Guide](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md)

### 3.3.2. Starting the printing

1. Insert the SD card where you saved the G-code
2. Select your file with the use the of the button
3. Wait for the preheating to finish
4. When the printing start check if it is printing correctly. There are three possibilities
     * If the filament is not adhering correctly it will float a little. It will screw up the printing
     * The filament is adhering correctly. This is what we want.
     * The printing head is to near to the plate and is crunching the print. This will also screw up the printing.

![Import](../images/Mod3/First-Layer.jpg){width="500"}

Source:

* [First Layer Issues](https://help.prusa3d.com/article/first-layer-issues_1804)
* [FabLab Guide Starting the Printing](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md#lancement-de-limpression)
* [FabLab Guide ADherence Issue](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md#mauvaise-adh%C3%A9rence-de-limpression-au-plateau)

### 3.3.3. Other Issues

Knot in the filament:

* [FabLab Guide](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md#n%C5%93uds-dans-la-bobine-de-filament)

Older Firmware on the Printer

* [FabLab Guide](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md#ancienne-version-du-slicerfirmware)
* [Prusa Guide for updating](https://help.prusa3d.com/article/firmware-updating-mk3s-mk3s-mk3_2227)

For more information you can always check the [Prusa Web Page](https://help.prusa3d.com/tag/mk3s-2)

### 3.3.4. Testing After Printing

<iframe width="560" height="315" src="https://youtu.be/FYEK3JQNL9Q" title="Spring Up-Down" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://youtu.be/Y2lkSczkymY" title="Spring Left-Right" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://youtu.be/SrT5yxx4P3M" title="Spring Front-Back" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## 3.4. Interesting things
### 3.4.1. 3D Printing a cake

<iframe width="560" height="315" src="https://www.youtube.com/shorts/fxQABNwwdas" title="3D Printing a cheesecake" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>