# Module 2. Computer-Aided Design (CAD)

## 2.1. Installing the different tools

I personally installed all of the following tools on my Windows 10 PC for a easier workflow.

### 2.1.1. Inkscape

If you want to create a image made of [vectors](https://en.wikipedia.org/wiki/Vector_graphics)
, Inkscape is for you.Instead of saving the image point to point, it saves vectors. The image format is an ```.svg```.

You can install Inkscape from the [following website](https://inkscape.org/).

### 2.1.2. OpenSCAD

If you want to create 3D objects and you are really familiar with coding, you can use OpenSCAD. You give him command lines and it transforms it to a 3D object.

You can install OpenSCAD from the [following website](https://openscad.org/).

### 2.1.3. FreeCAD

If you don't feel familiar with command lines oor you are more of a visual guy/girl, you can use FreeCAD do draw the object that you want to create. Beware that you will need to give it lots of constraints to your object to be a viable 3D object.

You can install FreeCAD from the [following website](https://www.freecad.org/).

## 2.2. Creating a Compliant Mechanism

### 2.2.1. What is a Compliant Mechanism

Our objective was to create a kit, in groups, that contains multiple compliant mechanisms

<iframe width="560" height="315" src="https://www.youtube.com/embed/97t7Xj_iBv0" title="Compliant Mechanisms explained by Veritasium" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### 2.2.2. Creating the shape

I wanted to create a spring. For this I used FreeCAD as it would be a complex shape.

Tips:

* Save a lot and also make duplicates with *save as*.

After multiple trial and error, this is how I created my **final** shape.

1. First we need to create a New Project. Click on **Create New...**
2. Then in the drop-down menu select **Part Design** option.
3. In the **Task** window click on **Create body**
4. Then **Create Sketch** (in the same window)
5. Select the coordinates in which you want to start. It will decide if your working plan will be the front/top/... of the object you are working to create.

![Creating project](../images/Mod2/step1.jpg){width="500"}

Before starting we need to know some rules for creating a shape.

* The final shape needs to have 0 [DoF (Degree of Freedom)](https://en.wikipedia.org/wiki/Degrees_of_freedom).
   * The number of DoF can be seen in the Task Window.
* The shape needs to be closed. If not the following error message will appear later on: ``` wire is not closed ```.
* Geometrical constraints are better than distance constraints.
   * More info on the different constraints can be found [here](https://wiki.freecad.org/Constraint)

Knowing this we can start working on the object itself.

1. I created the base of my shape using the square form.
2. Click on *Close* in the Task window.
3. In the drop-down menu select Spreadsheet than create one. In the spreadsheet :
   1.   write in the first cell the name / description of the variable
   2.   in the second cell write the value **AND** in the **Alias** box give it an alias
   3.   in the third cell write the alias given for an easier seeing

![Spreadsheet](../images/Mod2/step2.jpg){width="500"}

4. Giving constraints to my rectangle:
* using the symmetry constraint I center the rectangle on the center of my working plan. To do this, select two points (they will be green when selected) and a third one on which they will be mirrored. 
* using the distance constraints (vertical and horizontal) I link the values of my spreadsheet with the height and length of my box. To do this select on two points (they will become green when selected). A pop-up will appear. Click on the blue circle and type ```spreadsheet.``` and after the point write the alias that you have chosen before.

![Distance Constraint](../images/Mod2/step3.jpg){width="500"}

1. After this, I put a point on my box and gave it a distance constraint from the edge. This point is the starting point of the Spring part.
2. Then I started drawing in zig-zag to create the spring part while using the symmetry and distance constraints.
3. Recreated the same box above the spring.
4. Using the *Trim Edge* tool, I cut into the box where the spring was attached to make into one piece. I needed to add some constraints after the cut.

![Trim](../images/Mod2/step4.jpg){width="500"}

9. When I finished with the 2D design, I closed the sketch menu in the task window and change, in the drop-down menu to design part section. 
10. In the design part section select the Pad tool to create the 3D form.

![Pad](../images/Mod2/step5.jpg){width="500"}

11. Then I selected one of the faces of my 3D object and used the *create sketch* tool to edit that face
    * I added circle to make it like a lego and used several constraints to make it centered and parametrized the radius of it
    * Used the `Pad tool` to pop-it-out from the face
    * Used the `Pocket tool` to make the hole inside the form

![Pad](../images/Mod2/step6.jpg){width="500"}

12. When I was satisfied with my work I saved the file. The next step is described in the [Module 3](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/patrick.dezseri/fabzero-modules/module02/#33-creating-your-printable-file).

My file can be found [here](https://gitlab.com/patrick.dezseri/my-files/-/tree/main/3D-Objects/Spring).

I used the following help when I got stuck.

* [FreeCAD videos](https://gitlab.com/fablab-ulb/enseignements/fabzero/cad/-/blob/main/FreeCAD.md) from Nicolas H.-P. De Coster
* multiple google search
* FreeCASs' [documentations](https://wiki.freecad.org/Main_Page)
* [ChatGPT](https://chat.openai.com)
* [FreeCAD Forum](https://forum.freecad.org/index.php?sid=d01c12f4ecd75421d6a9f5aa1b8e0e97)

### 2.2.3. Example of questions ask to ChatGPT

![ChatGPT1](../images/Mod2/chatgpt1.png){width="500"}


## 2.3 Creative Commons 

Creative Common licenses allows the creator of the work to license / copyright the work itself.

There exist different type of CC Licensing 

| Abbreviation |  Description    |
|-----|-----------------|
| CC  | Means that it has a Creative Commons license on it    |
| BY  | Credit must be given to the creator    | 
| SA  | Adaptations must be shared under the same terms  | 
| NC  | Only noncommercial uses of the work are permitted   | 
| ND  | No derivatives or adaptations of the work are permitted  | 
| CC0 | The creators gives up their copyright and their works become public domain with no conditions |  

Multiple versions exist:

* CC BY
* CC BY-SA
* CC BY-NC
* CC BY-NC-SA
* CC BY-ND
* CC BY-NC-ND
* CC0

To copyright your work you just need to put the CC license in a visible way with your work.

You can read more about different licenses [here](https://www.gnu.org/licenses/license-list#SoftwareLicenses)

When using the code of someone else you can read how to cite correctly [here](https://integrity.mit.edu/handbook/writing-code)

Source :

* [CC](https://creativecommons.org/about/cclicenses/)
    * The CC Descriptions where copied from this site

## 2.4. How to use others people's work

1. First find the work that you are interested in.
2. Check what kind of CC the creator put on their work
3. If it has an ND you CAN NOT modify it, only use it as it is.
4. If it does not have ND you can modify it and give credits to the creator as described in [Chapter 2.3.](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/patrick.dezseri/fabzero-modules/module02/#23-creative-commons)
