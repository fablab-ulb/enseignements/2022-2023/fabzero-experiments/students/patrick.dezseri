# Module 5. Team Dynamics
## 5.1. Project Analysis
With my [support pair](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/damien.delattre/)
 we analyzed the following issue: **Plastics**

We came up with the following [Problem and Objective tree](https://www.youtube.com/watch?app=desktop&v=9KIlK61RInY)

![Problem Tree](../images/Mod5/problem-tree.png){width="250"}
![Solution Tree](../images/Mod5/solution-tree.png){width="250"}

To create these tree we did an online meeting and used OneNote, so we could write at the same time in the same document.

## 5.2. Group creation
### 5.2.1. The object

For the groupe creations, we were asked to bring an object that symbolizes a problem that is "dear" to us.

My object was a CPU that I have found in the level one of the campus of Plaine. This is where the university stores, temporarily, the object that will be throne away. 

![CPU](../images/Mod5/cpu.jpg){width="150"}

I brought this because in our times e-waste start to be a big issue and I would like to find a way the recycle electronic components. 

### 5.2.2. The group 
Than we were asked to put our object together. Look at them. And found partners with similar objects.

We created a group of 8-9 people, but this was too big. So we divided ourself into two groups.

I have found my self with

 * [Cosmin](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/cosmin.tudor/)
 who came with a microcontroller 

* [Dimitri](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/dimitri.debauque/)
 who came with a mobil phone

 * [Louis](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/louis.vanstappen/)
 who came with a headphone
 
[The group Page](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-01/)

### 5.2.3. The competition was on
The next task was with our group. We got a A2 (or was it A3) paper. We needed to write words that were connected with our objects.

The competition was on.

We were brainstorming as fast as we could and after 3 minutes we got around 67 words.

We found the most words (approximately 70 words)

Some of them are

* rarity of the rare minerals used 
* fast fashion of the gadget (CPU, phone, GPU,...)
* environmental issues (open mines, waste not recycled, toxicity of certain elements, ...)
* and a lot more

### 5.2.4. A ta place, je ... / In your place, I ...

After the word champion ship, we needed to discuss each of ours problematic, while starting our phrases with *In your place, I...*. 

After some time we continued this, but with a variant.One of us needed to go to an other group and tell them the problematic and the other group would start a discussion with the same phrase. Meanwhile the three that stayed behind would do the same with someone from an other group.

Than the moving person would come home and explain what he was told.

End of the day.

## 5.3. Team dynamic tools
### 5.3.1. The weather
In this method we ask the people who are present how motivated / energetic they are. Instead of responding one by one everybody raises/points their hand to convey their energy level.

For example if it points down to the legs, it means that they have 0 energy. If it is raised up above their head, they are full of energy. And everything in between is proportional to their energy.

Tip: shake the hand while doing pointing so people outside wont get the wrong impression. 

### 5.3.2. What's Up Doc ?
We do a round table discussion to know whats up with everyone. This might help not to get the wrong impression if someone has a bad day.

For example. You are presenting your latest results and someone in the meeting is absent-minded. You might take this wrongly as your presentation is annoying. BUT if you did a *whats up doc* round, just before starting the presentation, you would know that their dog died yesterday.

### 5.3.3. Voting

*Vote without objection* is when a something is voted without everyone being for it but no one is against it.

[*Majority judgment*](https://app.mieuxvoter.fr/) is a type of vote when you vote on a scale from (example) 0 (you are against it) to 10 (for it 100%) and everything in between. 

