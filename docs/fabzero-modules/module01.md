# Module 1. Project management and documentation
## 1.1. Setup Process

First step was to setup Git,MKDocs and other programs on my laptop/PC

### 1.1.1. For Ubuntu

I installed Git using the following lines in the terminal (picture bellow)

![Terminal](../images/Mod1/terminal.png){width="250"}

```
sudo apt-add-repository ppa:git-core/ppa
sudo apt-get update
sudo apt-get install git
```

Then I needed to configure it.

```
git config --global user.name "your_username"
git config --global user.email "your_email_address@example.com"
```

Then I needed to found my public SSH key. To find it I did the following

1. in the file explorer use the following key combination *ctrl + H* to reveal hidden files
2. found the .ssh

I did not have a public SSH key in my folder so I needed to generate one. In the terminal, I have typed
```ssh-keygen```. This created an *id_rsa.pub* file in which was my public SSH key. This key I copy-pasted in the settings of my gitlab profile.

Then I copied my repository on my laptop with 
``` 
git clone url
```
where the url is replaced by the copied text from the gitlab project, given by *Clone with SSH*

In the cloned project, there was a *readme.md* file which told us to install MKDocs. For this I first needed to install python 3.

```
sudo apt install python3
sudo apt -y install mkdocs
sudo apt install python3-pip
pip install mkdocs-bootswatch
```
For previewing your work you can use the following command
```
mkdocs serve
```
and using the given link you can follow the changes you make in real time.

For everyday uses (when the terminal is **open in the right directory**)

Download the latest changes in the project
```
git pull
```

Add all changes to commit
```
git add -A
git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"
```

Send changes to remote server
```
git push
```

Source:

* [Install Python](https://www.makeuseof.com/install-python-ubuntu/)
* [Install mkdocs](https://installati.one/install-mkdocs-ubuntu-22-04/)
* [Install Bootswatch](https://mkdocs.github.io/mkdocs-bootswatch/)
* [User Guide](https://www.mkdocs.org/user-guide/installation/)
* [mkdocs Getting Started](https://www.mkdocs.org/getting-started/)
* [FabLab Guide](https://gitlab.com/fablab-ulb/enseignements/fabzero/basics/-/blob/main/documentation.md)
* [installing Git](https://docs.gitlab.com/ee/topics/git/how_to_install_git/index.html)
* [Setup SSH key](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-20-04)
* [Using Git](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git)
* and thanks for the help Damien.

### 1.1.2. For windows with a Virtual Machine (VM)
Note:
* When I say restart it means the Virtual Computer and not the computer you are working on.

The first step is to download the install (iso) file for Ubuntu.  You can find them here:

[Download Ubuntu](https://ubuntu.com/download/desktop)

This is the longest step as it can take a longtime to download.


While the iso file for ubuntu is downloading you can **download** and **install**: VirtualBox (VB) **and** the Oracle VM VirtualBox Extension Pack from:

[Download VirtualBox](https://www.virtualbox.org/)

When VB is installed and the iso file for ubuntu is downloaded:

1. Launch VB and click on New in the menu. A new windows should pop-up

![VMlaunchSetup](../images/Mod1/VM1.jpg){width="500"}

2. In the new window 
    1. for ```Name``` give a name (you can give Ubuntu)
    2. for ```Folder``` choose where you want to create your virtual PC
    3. for ```ISO Image``` search for the ubuntu iso install file downloaded at the beginning
    4. Next

![VMlaunchSetup1](../images/Mod1/VM2.jpg){width="500"}

3. Choose an username and a password. 
4. Check the Guest Additions box. Next

![VMlaunchSetup2](../images/Mod1/VM3.jpg){width="500"}

5. Give your system the hardware requirements. This depends on your computer that you are using. I recommend giving (if it is still in the green lines)
    1. 8GB of Base Memory (8192 MB)
    2. 4 CPUs
    3. Next
6. `Create the Virtual Hard Disk Now` with a minimum of 10GB. Next
7. Finish. And let the computer do its thing. You might need to click on the Start button (Green Arrow). When prompted give your password. For laptop users: Be sure to have enough battery power throughout the process.

If prompted to update make sure to have enough power (if on laptop) and time, but you can do the update later.

Try opening the Terminal. If you see loading but it does not open do the following.

1. go into settings --> Language & Regions 
2. choose another language than the default (it can be an other english form)
3. choose restart now

![Bug1](../images/Mod1/bug1.jpg){width="500"}

Now you can follow the steps of 1.1.1. In the case you have the following error ```Username is not in the sudoers file. This incident will be reported``` do the following.

1. restart the computer
2. smash ```shift```-key like your life depends on it
3. in the menu choose ```Advanced options for Ubuntu```

![Bug2](../images/Mod1/bug21.png){width="500"}

4. Choose the ```recovery mode``` of the latest version

![Bug2bis](../images/Mod1/bug22.png){width="500"}

5. Choose ```root```

![Bug2tris](../images/Mod1/bug23.png){width="500"}

6. Enter your password and press enter (the password wont show-up while writing)
7. Write the following:
```
mount -o rw,remount /
adduser username sudo
exit 
```
8. Choose ```resume```
9. See if you can follow the steps of 1.1.1. If not go into my sources.

**Optional steps:**

When you launched ubuntu in the VM.

1. In the menubar (of the VM machine) choose Device --> Insert Guest Additions CD image...
2. In your file explorer in Ubuntu, a CD will show up.
3. Right click on ```autorun.sh``` and choose ```Run as Program```
4. If it does nothing you can do the same on ```runasroot.sh``` and ```VBoxLinuxAdditions.run```

**Optioanl - *Drag'n Drop* and the *Shared Clipboard* option**

1. For optimal workflow you can also enable in ```Devices``` the ```Shared Clipboard``` and ```Drag'n Drop``` option or the ```Shared Folders```

Source:

* [Installing Ubuntu on VM](https://ubuntu.com/tutorials/how-to-run-ubuntu-desktop-on-a-virtual-machine-using-virtualbox#1-overview)
* [Resize windows screen](https://www.ghacks.net/2022/06/11/how-to-change-the-windows-screen-size-in-virtualbox/)
* [Terminal Issue](https://askubuntu.com/questions/1435918/terminal-not-opening-on-ubuntu-22-04-on-virtual-box-7-0-0)
* [Add sudoer](https://www.tecmint.com/fix-user-is-not-in-the-sudoers-file-the-incident-will-be-reported-ubuntu/)

## 1.2. Writing 
### 1.2.1. For mkdocs

I searched and found the following sites for formatting in mkdocs:

* [mkdocs Guide Writing](https://www.mkdocs.org/user-guide/writing-your-docs/)
* [mkdocs material](https://squidfunk.github.io/mkdocs-material/reference/lists/#using-unordered-lists)
* [Cheat Sheet](https://www.markdownguide.org/cheat-sheet)

If you want to leave a comment in your text, that is only visible for the editors (not the readers) you can use the following:

```

[comment]: # (Write your comment here)

```

The empty line before and after the comment is necessary

* [StackOverflow](https://stackoverflow.com/questions/4823468/comments-in-markdown)

### 1.2.2. Editor
There exist several text editor. I choose [Visual studio Code]()
because I was already familiar with it and I found it easy to use. We can also add really easely different addons, like spellcheck, etc.

## 1.3. Home Page Setup

I first looked in the ```mkdocs.yml``` file and replaced different sections with my name. Then I search for what the themes are. Here are two web pages with the previews:

* [Preview 1](https://mkdocs.github.io/mkdocs-bootswatch/)
* [Preview 2](https://bootswatch.com/)

To modify the Table of Contents, we can look at [cheat sheet](https://python-markdown.github.io/extensions/toc/)
of the toc extension.

## 1.4. Picture Things

It is important to reduce the picture size to keep the storage size under control

### 1.4.1. Graphics Magick
I installed GraphicsMagick using the following command
```
sudo apt install graphicsmagick
```
Opened the terminal in the folder containing my pictures.

* run the following command to resize 

```
gm convert -resize 600x600 bigimage.png smallimage.jpg
```

To insert an image, the following command can be used. Be sure to safe first the image in the correct folder. (Tip: it is in the ```images``` folder, be aware that it is not the same for the modules and the home page):
```
![Title](images/avatar-photo.jpg)
```

Source:

* [Graphic Magick Home Page](http://www.graphicsmagick.org/index.html)

### 1.4.2. Image formatting in text

For more image formatting in my texts I added a plugin glightbox with the following command:
```
pip install mkdocs-glightbox
```
I don't know if it will work or not :| 

Source: 

* [Glightbox](https://squidfunk.github.io/mkdocs-material/reference/images/)

### 1.4.3. GIMP

### 1.4.4. Inserting an image 

To insert an image you can use this formating:

```
![Title](../images/Mod1/image_name.png){width="500"}
```

For ../images... it is the folder where you save your image.

Make sure that you put the right image format in the line code (jpg is not the same as png or jpeg), but most image format are accepted.

```{width="350"}``` is optional. It is just to size the image on the web page.

## 1.5. Videos
Like for pictures,it is important to keep the video size under control.

### 1.5.1. FFMPEG

To install we can use the following command:
```
sudo apt install ffmpeg
```
I will update this section when I will use ffmpeg in the future.


Source:

* [ffmpeg Home Page](https://ffmpeg.org/)
* [Install](https://linuxize.com/post/how-to-install-ffmpeg-on-ubuntu-20-04/)

### 1.5.2. Linking a Video from Youtube 

To link a video you can use the following command

```
<iframe width="560" height="315" src="https://www.youtube.com/embed/Q3oItpVa9fs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
```

## 1.6. Embed file in mkdocs

* [Stackoverflow](https://stackoverflow.com/questions/69237778/how-to-embed-a-local-pdf-file-in-mkdocs-generated-website-on-github-pages)


## 1.7. Other 

Sites that can be useful later on:

* [Docker](https://hub.docker.com/r/minidocks/mkdocs#!)
* [FabLab Documentation](https://gitlab.com/fablab-ulb/enseignements/fabzero/basics/-/blob/main/documentation.md)
