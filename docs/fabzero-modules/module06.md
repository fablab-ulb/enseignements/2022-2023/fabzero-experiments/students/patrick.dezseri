# Correcting Pipe line issue

1. Go to your GitLab group project on the GitLab web page
2. Click on `CI/CD configuration button`

![Where](../images/ModPipeLine/CI-CD.png){width="500"}

3. Add a space before `only` so it is on the same line as `artifacts`


Problematic line is underlined in red:

![Wrong](../images/ModPipeLine/wrong.png){width="700"}


The correct is :

![Correct](../images/ModPipeLine/correct.png){width="700"}

4. Click on the button of `Commit Changes` just bellow the code

# Run the pipeline

1. Go into the CI/CD side bar menu

![Side Menu](../images/ModPipeLine/pipe1.png){width="150"}

2. The easiest way is to go unto the `Schedules` submenu and run a scheduled pipeline run

![Side Menu](../images/ModPipeLine/pipe2.png){width="700"}

3. Go into the `Pipelines` submenu and you can follow if it went through or not (it can take several minutes before getting the cross or checkmark)

![Side Menu](../images/ModPipeLine/pipe3.png){width="700"}

# To check if it worked

To check if it worked you can just go to your group page, where you replace **01** with your group number

[https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-01/](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-01/)