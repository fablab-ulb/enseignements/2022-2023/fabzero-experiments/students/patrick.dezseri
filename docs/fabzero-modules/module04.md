# Module 4. FabLab Tools - Repair Shop
## 4.1. Introduction 
The introduction to this module was a woden box with a witch. In the box there was a circuit with a battery, a led, and the switch. We needed to find why the led wasn't turning on. For this we disassembled the box that had different type of screws and search with a multimeter where the issue could be. 

For my box, it was a broken cable.

## 4.2. Electronics
Than we had a quick introduction to different electronic components.

**Batteries**

 * Most commons are the lithium ion

**Capacitors**

* Ceramic
* Polymer 
* 

They come with different sizes and forms.

**Fuses**

* electrical
* heat

**Led**


**Resistors**

* Constant resistors 
* Variable resistors / potentiometers

**Semiconductors**

* the current can only go one way

## 4.3. Repair
### 4.3.1. My broken object
I have a broken laptop case that I wanted to repair but it was to complicated to design it.

### 4.3.2. Other broken object - The Oven knob

So the teacher also brought broken objects and we (as we did the repairs in groups of two) choose a broken knob of an oven.

![Broken](../images/Mod4/broken.jpg){width="150"}

After measuring the parameters we recreated a simplified model in FreeCAD. 

The file can be found [here](https://gitlab.com/patrick.dezseri/my-files/-/tree/main/3D-Objects/Knob-Oven)

Than we printed it and gave it to the teacher who knew the person who needed the repair.

![Repair Prototype](../images/Mod4/repair.jpg){width="150"}

As this one the first prototype it is not guaranteed that it will work and as such it was printed with the 0.3mm draft parameter.

#### 4.3.2.1. Analysis

We used a [calliper](https://en.wikipedia.org/wiki/Calipers)
to measure the parameters of the broken knob. 

#### 4.3.2.2. CAD Design
1. We first created the handle. A simple box. 
2. Than we created a cylinder with the correct hight
3. Than we carved out the shape where the knob meets the oven handle.
   *  For the first prototype we did not make it exactly as the broken part (because of our free time). The only difference is that in the original knob the circle is cut at one end. We did not make the cut and so we had a perfect circle instead.

#### 4.3.2.3. Quality testing

Due to miscommunication and the easter holidays we could do a quality check. 